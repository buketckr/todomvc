ToDoList Item Islemleri
=======================


ToDo Listesine Item Ekleme
------------------------------
tags:todo_item_ekleme

*"https://todomvc.com/examples/vue/" adresine gidilir
*Todo listesinin bos oldugu kontrol edilir "toDoListKontrol"
*"itemGirisKutusu" alanina "Proje yapilacak" itemi eklenir
*"Proje yapilacak" iteminin listeye eklendiği kontrol edilir "itemKontrol"

ToDo Listesine Birden Fazla Item Ekleme ve Dogrulama
------------------------------
tags:todo_item_ekleme_dogrulama

*"https://todomvc.com/examples/vue/" adresine gidilir
*"Proje yapilacak" iteminin listeye eklendiği kontrol edilir "itemKontrol"
*"itemGirisKutusu" alanina "Bilgisayari al" eklenir
*"Bilgisayari al" iteminin listeye eklendiği kontrol edilir "itemKontrol"
*"Bilgisayarı al" iteminin "Proje yapilacak" iteminin altına eklendiği kontrol edilir "itemKontrol"

ToDo Listesinde Radio Buttona Tiklama
--------------------------------
tags:todo_butona_tikla

*"https://todomvc.com/examples/vue/" adresine gidilir
*Listede "Proje yapilacak" ve "Bilgisayari al" itemlerinin oldugu kontrol edilir "itemKontrol"
*"Proje yapilacak" itemi icin yanindaki "radioButton" butonuna tiklanir "itemKontrol"
*"Proje yapilacak" iteminin mark edildigi kontrol edilir "itemKontrol" "toDoListesi"

ToDo Listesinde Radio Buttona Tiklamayi Kaldirma
---------------------------------------
tags:todo_butona_tiklamayi_kaldir

*"https://todomvc.com/examples/vue/" adresine gidilir
* "toDoListMarked" listede item var mi kontrol edilir
* "Proje yapilacak" itemi icin yanindaki "radioButton" butonuna tiklanir "itemKontrol"
* "Proje yapilacak" iteminin mark edildigi kontrol edilir "itemKontrol" "toDoListesi"

ToDo Listesinde Item Silme
-----------------------------
tags:todo_item_sil

*Listede "Proje yapilacak" ve "Bilgisayari al" itemlerinin oldugu kontrol edilir "itemKontrol"
*"itemGirisKutusu" alanina "Her seyi duzenli yap" eklenir
*"Her seyi duzenli yap" iteminin listeye eklendiği kontrol edilir "itemKontrol"
* "Proje yapilacak" itemi icin yanindaki "tolistDelete" butonuna tiklanir "itemKontrol"