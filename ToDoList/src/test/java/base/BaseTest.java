package base;

import com.thoughtworks.gauge.*;
import helper.ElementHelper;
import helper.StoreHelper;
import model.ElementInfo;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.ArrayList;
import java.util.List;

public class BaseTest {

    @BeforeSuite
    public void hazirlik() {
        System.out.println("Senaryo Basladi-----------");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        action = new Actions(driver);
        driver.manage().window().maximize();
    }

    public static void getUrl(String url) {
        driver.get(url);
    }

    static WebDriver driver;
    static Actions action;

    @AfterSuite
    public void bitir() {
        driver.quit();
        System.out.println("Senaryo Sonlandi------");
    }

    public WebElement findElement(String key) {
        ElementInfo elementInfo = StoreHelper.INSTANCE.findElementInfoByKey(key);
        By infoParam = ElementHelper.getElementInfoToBy(elementInfo);
        WebDriverWait webDriverWait = new WebDriverWait(driver, 20);
        WebElement webElement = webDriverWait
                .until(ExpectedConditions.presenceOfElementLocated(infoParam));
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'})",
                webElement);
        return webElement;
    }

    public List<WebElement> findElements(String key){
        ElementInfo elementInfo = StoreHelper.INSTANCE.findElementInfoByKey(key);
        By infoParam = ElementHelper.getElementInfoToBy(elementInfo);
        return driver.findElements(infoParam);
    }

    @Step("<saniye> saniye beklenir")
    public void waitElement(int key) throws InterruptedException {
        Thread.sleep(key*1000);
        System.out.println(key+ "Saniye beklendi");
    }

    public void clickListElement(String by,int index){
        findElements(by).get(index).click();
    }

    public void enterClick(String by){
        findElement(by).sendKeys(Keys.ENTER);
    }

    public  void sendkeysElement(String by,String text){
        findElement(by).sendKeys(text);
    }

    public void assertControls(String assertName, String expectedName){
        List<String> list = itemListesi(assertName);
        Assert.assertTrue("Element var"+expectedName,list.contains(expectedName));
    }

    public static String convertTurkishChar(String string) {
        string = string.replace("ç", "c");
        string = string.replace("ö", "o");
        string = string.replace("ş", "s");
        string = string.replace("ğ", "g");
        string = string.replace("ü", "u");
        string = string.replace("ı", "i");
        string = string.replace("Ç", "C");
        string = string.replace("Ö", "O");
        string = string.replace("Ş", "S");
        string = string.replace("Ğ", "G");
        string = string.replace("Ü", "U");
        string = string.replace("İ", "I");
        return string;
    }

    public List<String> itemListesi(String key){
        List<WebElement> todoList = findElements(key);
        List<String> list = new ArrayList<>();
        for (WebElement i: todoList) {
            list.add(i.getText());
        }
        return list;
    }

    public  boolean markEtmek(String key1, String key2, String key3){
        WebElement todo=findElement(key2);
        List<WebElement> todoList = todo.findElements(By.tagName("li"));
        List<String> list= itemListesi(key3);
        int index = list.indexOf(key1);
        String textmark = todoList.get(index).getAttribute("class");
        String markE = "todo completed";
        return (textmark.equals(markE));
    }
}