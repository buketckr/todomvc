package step;

import base.BaseTest;
import com.thoughtworks.gauge.Step;
import java.util.List;

public class StepImplementation extends BaseTest {

    @Step("<url> adresi acilir")
    public void adreseGit(String url) {
        getUrl(url);
        System.out.println("Sayfaya giris yapildi");
    }

    @Step("<item> listede item var mi kontrol edilir")
    public void ToDoListesiKontrolu(String item) {
        try {
            findElement(item);
            System.out.println("Listede item var");
        } catch (Exception e) {
            System.out.println("Listede item yok");
        }
    }

    @Step("<itemGirisKutusu> alanina <yeniItem> eklenir")
    public void yeniItemEkleme(String itemGirisKutusu, String yeniItem) {
        sendkeysElement(itemGirisKutusu, yeniItem);
        enterClick(itemGirisKutusu);
    }

    @Step("<itemKontrol> item eklendi mi kontrol edilir <yeniItem>")
    public void itemEklemeKontrol(String itemKontrol, String yeniItem) {
        assertControls(itemKontrol,yeniItem);
        System.out.println(yeniItem + "elementi listede vardır");
    }

    @Step("<itemB> itemi <itemA> altindami <itemKontrol>")
    public void itemAltUstKontrolu(String itemB, String itemA, String itemKontrol) {
        int itemBint = itemListesi(itemKontrol).indexOf(itemB);
        int itemAint = itemListesi(itemKontrol).indexOf(itemA);

        if (itemBint > itemAint) {
            System.out.println("Altinda");
        } else {
            System.out.println("Uzerinde");
        }
    }

    @Step("<Proje yapilacak> itemi icin <radioButton> butonuna tiklanir <itemKontrol>")
    public void radioButon(String key1, String key2, String key3) {
        String text = key1;
        List<String> list = itemListesi(key3);
        for (int i = 0; i < list.size(); i++) {
            if (text.equals(list.get(i))) {
                clickListElement(key2, i);
                System.out.println("Elemente Tiklandi *****"+text);
            }
        }
    }

    @Step("<key1> item mark kontrolu <itemKontrol> <key2>")
    public void markEtme(String key1, String key3, String key2) {
        System.out.println("<*");
        if (markEtmek(key1, key2, key3) == true) {
            System.out.println("<*<");
            System.out.println("Mark edildi");
        } else
            System.out.println("Mark edilmedi");
    }
}